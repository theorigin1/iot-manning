﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace IOT.Server.Hubs
{
    public class IotClient
    {
        public IotClient(string clientId, string connectionId, DateTime lastHeartBeat)
        {
            ClientId = clientId;
            ConnectionId = connectionId;
            LastHeartBeat = lastHeartBeat;
        }

        public string ClientId { get; }
        public string ConnectionId { get; }
        public DateTime LastHeartBeat { get; set; }
    }

    public class QueuedAudio
    {
        public string ConnectionId { get; }
        public string AudioFile { get; }

        public QueuedAudio(string connectionId, string audioFile)
        {
            ConnectionId = connectionId;
            AudioFile = audioFile;
        }
    }

    public class IotHub : Hub
    {
        private const string ManagerGroup = "Manager";

        private static readonly List<IotClient> IotClients = new List<IotClient>();
        private bool _audioIsPlaying;

        private static Queue<QueuedAudio> _queuedAudio = new Queue<QueuedAudio>();

        public void Heartbeat(string clientId)
        {
            var iotClient = GetClientFromClientId(clientId);

            if (iotClient == null)
                return;
                
            iotClient.LastHeartBeat = DateTime.UtcNow;

            Clients.Group(ManagerGroup).SendCoreAsync("HeartBeatReceived", new object[] { iotClient.ClientId, iotClient.LastHeartBeat });
        }

        public void Connected(string clientId)
        {
            var iotClient = new IotClient(clientId, Context.ConnectionId, DateTime.UtcNow);
            IotClients.Add(iotClient);
            
            Clients.Group(ManagerGroup).SendCoreAsync("Connected", new object[] { iotClient.ClientId, iotClient.LastHeartBeat });
        }

        public void RequestToPlay(string audioFile)
        {
            if (!_audioIsPlaying)
            {
                Clients.Caller.SendAsync("PlayAudio", audioFile);
            }

            QueueAudio(audioFile, Context.ConnectionId);
        }

        private static void QueueAudio(string audioFile, string connectionId)
        {
            _queuedAudio.Enqueue(new QueuedAudio(connectionId, audioFile));
        }

        public void FinishedPlaying()
        {
            _audioIsPlaying = false;

            PlayQueued();
        }

        private void PlayQueued()
        {
            if (!_queuedAudio.TryDequeue(out var nextToPlay))
                return;

            Clients.Client(nextToPlay.ConnectionId).SendAsync("PlayAudio", nextToPlay.AudioFile);
        }
        
        public void RegisterManager(string clientId)
        {
            var iotClient = GetClientFromClientId(clientId);
            Groups.AddToGroupAsync(iotClient.ConnectionId, ManagerGroup);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var iotClient = GetClientFromConnectionId(Context.ConnectionId);

            IotClients.Remove(iotClient);

            Clients.Group(ManagerGroup).SendCoreAsync("Disconnected", new object[] { iotClient.ClientId });

            return base.OnDisconnectedAsync(exception);
        }

        private static IotClient GetClientFromConnectionId(string connectionId)
        {
            return IotClients.FirstOrDefault(x => x.ConnectionId.Equals(connectionId, StringComparison.OrdinalIgnoreCase));
        }

        private static IotClient GetClientFromClientId(string clientId)
        {
            return IotClients.FirstOrDefault(x => x.ClientId.Equals(clientId, StringComparison.OrdinalIgnoreCase));
        }
    }
}