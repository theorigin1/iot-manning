﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using IOT.Server.Hubs;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IOT.Server
{
    internal class TimedHostedService : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IHubContext<IotHub> _hubcontext;
        private Timer _timer;
        private TimerState _timerState = new TimerState();

        public TimedHostedService(ILogger<TimedHostedService> logger, IWebHostEnvironment webHostEnvironment, IHubContext<IotHub> hubcontext)
        {
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            _hubcontext = hubcontext;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            _timer = new Timer(DoWork, _timerState, TimeSpan.Zero,
                TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");

            var flights = File.ReadAllLines(Path.Combine(_webHostEnvironment.WebRootPath, "data", "flights.csv"))
                .Skip(1)
                .Select(line => line.Split(','))
                .Select(flight => new Flight(
                    int.Parse(flight[0]),
                    int.Parse(flight[1]),
                    int.Parse(flight[2]),
                    int.Parse(flight[3]),
                    int.Parse(flight[4]),
                    int.Parse(flight[5]),
                    int.Parse(flight[6]),
                    flight[17])
                ).ToList();

            TriggerArrivals(flights);
            TriggerGateOpen(flights);
            TriggerFinalCall(flights);

            ((TimerState) state).LastCheck = DateTime.UtcNow;
        }

        private void TriggerFinalCall(IEnumerable<Flight> flights)
        {
            Trigger("FinalCall", time => flights.Where(x => x.FinalCall.Equals(time)).ToList());
        }

        private void TriggerGateOpen(IEnumerable<Flight> flights)
        {
            Trigger("GateOpen", time => flights.Where(x => x.GateOpen.Equals(time)).ToList());
        }

        private void TriggerArrivals(IEnumerable<Flight> flights)
        {
            Trigger("FlightArrival", time => flights.Where(x => x.Arrival.Equals(time)).ToList());
        }

        private void Trigger(string eventToTrigger, Func<DateTime, List<Flight>> func)
        {
            var now = DateTime.UtcNow;
            var nowNoSeconds = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, DateTimeKind.Utc);

            var matchingFlights = func.Invoke(nowNoSeconds);

            // create MP3 audio file here :-)
            var audio = File.ReadAllBytes(Path.Combine(_webHostEnvironment.WebRootPath, "data", "seatbelton.mp3"));

            foreach (var flight in matchingFlights)
            {
                _hubcontext.Clients.All.SendAsync(eventToTrigger, flight.Destination,audio);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }

    class TimerState
    {
        public DateTime? LastCheck;
    }

    class Flight
    {
        public string Destination { get; }

        public Flight(int year, int month, int dayOfMonth, int dayOfWeek, int depTime, int crsDepTime, int arrTime, string destination)
        {
            Destination = destination;
            var monthPadded = month.ToString().PadLeft(2, '0');
            var dayPadded = dayOfMonth.ToString().PadLeft(2, '0');
            
            var depTimePadded = $"{year}-{monthPadded}-{dayPadded} {depTime.ToString().PadLeft(4, '0')}";
            var arrTimePadded = $"{year}-{monthPadded}-{dayPadded} {arrTime.ToString().PadLeft(4, '0')}";

            Arrival = DateTime.ParseExact(arrTimePadded, "yyyy-MM-dd HHmm", null);
            GateOpen = DateTime.ParseExact(depTimePadded, "yyyy-MM-dd HHmm", null).AddHours(-1);
            FinalCall = DateTime.ParseExact(depTimePadded, "yyyy-MM-dd HHmm", null).AddMinutes(-30);
        }

        public DateTime GateOpen { get; }

        public DateTime FinalCall { get; }

        public DateTime Arrival { get; }
    }
}