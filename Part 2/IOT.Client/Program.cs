﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using NetCoreAudio;

namespace IOT.Client
{
    class Program
    {
        private static string _clientId = Guid.NewGuid().ToString();
        private const string HubUrl = "http://localhost:9967/TestHub";
        private static HubConnection _connection;
        private static Timer _timer;
        
        static async Task Main(string[] args)
        {
            Output("starting");
            
            _clientId = args.Length != 0 ? args[0] : _clientId ;

            Console.Title = _clientId;

            _connection = CreateHubConnection();
            
            await _connection.StartAsync();

            SetupEvents();

            Connected();

            SetupHeartbeat();

            Console.Read();

            await _timer.DisposeAsync();
        }

        private static void SetupEvents()
        {
            _connection.On<string, byte[]>("FlightArrival", (destination, audio) =>
            {
                Console.WriteLine("Flight arrived " + destination);
                PlayAudio(audio);
            });

            _connection.On<string, byte[]>("GateOpen", (destination, audio) =>
            {
                Console.WriteLine("Gate open " + destination);
                PlayAudio(audio);
            });

            _connection.On<string, byte[]>("FinalCall", (destination, audio) =>
            {
                Console.WriteLine("Final call " + destination);
                PlayAudio(audio);
            });
        }
        
        private static void PlayAudio(byte[] audio)
        {
            var tempFilename = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".mp3");

            File.WriteAllBytes(tempFilename, audio);

            var player = new Player();

            player.Play(tempFilename).Wait();
        }

        private static Task ConnectionOnReconnected(string arg)
        {
            Connected();

            return Task.CompletedTask;
        }
        
        private static void Connected()
        {
            Output("connected");
            _connection.InvokeAsync("Connected", _clientId);
        }

        private static void Disconnected()
        {
            Output("disconnected");
            _connection.InvokeAsync("Disconnected", _clientId);
        }

        private static HubConnection CreateHubConnection()
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl(HubUrl)
                .WithAutomaticReconnect()
                .Build();

            hubConnection.Reconnected += ConnectionOnReconnected;

            return hubConnection;
        }

        private static void SetupHeartbeat()
        {
            _timer = new Timer(
                callback: HeartbeatTask,
                state: null,
                dueTime: 5000,
                period: 30000);
        }

        private static void HeartbeatTask(object timerState)
        {
            Output("sending heartbeat");
            _connection.InvokeAsync("Heartbeat", _clientId);
        }

        public static void Output(string message)
        {
            Console.WriteLine($"{DateTime.UtcNow:HH:mm:ss.fff}: {message}.");
        }
    }
}