﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace IOT.Client
{
    class Program
    {
        private static string _clientId = Guid.NewGuid().ToString();
        private const string HubUrl = "http://localhost:9967/TestHub";
        private static HubConnection _connection;
        private static Timer _timer;
        
        static async Task Main(string[] args)
        {
            Output("starting");

            try
            {
                _clientId = args.Length != 0 ? args[0] : _clientId ;

                Console.Title = _clientId;

                _connection = CreateHubConnection();
                
                await _connection.StartAsync();

                Connected();

                SetupHeartbeat();

                Console.Read();

                await _timer.DisposeAsync();
            }
            finally
            {
                Disconnected();
            }
        }

        private static Task ConnectionOnReconnected(string arg)
        {
            Connected();

            return Task.CompletedTask;
        }
        
        private static void Connected()
        {
            Output("connected");
            _connection.InvokeAsync("Connected", _clientId);
        }

        private static void Disconnected()
        {
            Output("disconnected");
            _connection.InvokeAsync("Disconnected", _clientId);
        }

        private static HubConnection CreateHubConnection()
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl(HubUrl)
                .WithAutomaticReconnect()
                .Build();

            hubConnection.Reconnected += ConnectionOnReconnected;

            return hubConnection;
        }

        private static void SetupHeartbeat()
        {
            _timer = new Timer(
                callback: HeartbeatTask,
                state: null,
                dueTime: 5000,
                period: 30000);
        }

        private static void HeartbeatTask(object timerState)
        {
            Output("sending heartbeat");
            _connection.InvokeAsync("Heartbeat", _clientId);
        }

        public static void Output(string message)
        {
            Console.WriteLine($"{DateTime.UtcNow:HH:mm:ss.fff}: {message}.");
        }
    }
}