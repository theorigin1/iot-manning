﻿"use strict";

var app = new Vue({
    el: '#app',
    data: {
        clientId: "manager" + Math.floor(Math.random() * 1000 + 1),
        connections: []
    },
    methods: {
    },
    mounted: function () {
        var self = this;
        var connection = new signalR.HubConnectionBuilder().withUrl("/testhub").build();
        
        connection.on("HeartBeatReceived", function (clientId, heartbeatDateTime) {
            console.log('heartbeat received ' + clientId + ' - ' + heartbeatDateTime);

            updateConnection(clientId, heartbeatDateTime, "connected");
        });

        connection.on("Disconnected", function (clientId) {
            console.log('Client disconnected ' + clientId);

            var index = getIndexOfExistingClient(clientId);

            if (index !== -1) {
                updateConnection(clientId, self.connections[index].lastHeartbeatDateTime, "disconnected");
            }

        });

        connection.on("Connected", function (clientId, connectedDateTime) {
            console.log('Client connected ' + clientId);

            var index = getIndexOfExistingClient(clientId);

            if (index !== -1) {
                updateConnection(clientId, connectedDateTime, "reconnected");
            } else {
                updateConnection(clientId, connectedDateTime, "connected");
            }
        });

        connection.start().then(function () {
            console.log('connection started');

            connection.invoke("Connected", self.clientId).catch(function (err) {
                return console.error(err.toString());
            });

            connection.invoke("RegisterManager", self.clientId).catch(function (err) {
                return console.error(err.toString());
            });
        }).catch(function (err) {
            return console.error(err.toString());
        });

        function updateConnection(clientId, heartbeatDateTime, status) {
            var index = getIndexOfExistingClient(clientId);

            if (index === -1) {
                self.connections.push({ clientId: clientId, lastHeartbeatDateTime: heartbeatDateTime, status: status });
            } else {
                self.$set(self.connections, index, { clientId: clientId, lastHeartbeatDateTime: heartbeatDateTime, status: status });
            }
        }

        function getIndexOfExistingClient(clientId) {
            return self.connections.findIndex((e) => e.clientId === clientId);
        }
    }
});